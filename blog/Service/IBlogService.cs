using System.Collections.Generic;
using MauricioBlog.Models;

namespace MauricioBlog.Controllers
{
    public interface IBlogService
    {
        List<BlogPost> GetLatestPosts();
        string GetPostText(string link);
        List<BlogPost> GetOlderPosts(int oldestBlogPostId);
    }
}